function validateForm()
    {
    var firstname=document.getElementById('firstname').value;
    var address=document.getElementById('address').value;
    var pword=document.getElementById('password').value;
    var cpassword=document.getElementById('cpassword').value;
    var phoneno=document.getElementById('phoneno').value;
    var email=document.getElementById('email').value;
    var s1=document.getElementById('city').value;
    var radio1=document.getElementById('male').checked;
    var radio2=document.getElementById('female').checked;
    
    if (firstname=="")
    {
    alert("Name compulsory");
    return false;
    }
    if(!isNaN(firstname))
    {
        alert("only characters are allowed");
        return false;
    }
    
    if (address=="")
    {
    alert("address must be filled out");
    return false;
    }
    
    if (pword=="")
    {
    alert("please fill the password");
    return false;
    }
    
    if((pword.length<=5) || (pword.length>10))
    {
    alert("password length must be between 0 & 10");
    return false;
    }
    
    if(cpassword=="")
    {
    alert("please fill the confirm password");
    return false;
    }
    
    if(pword!=cpassword)
    {
    alert("password are not matching");
    return false;
    }
    
    
    if(s1=="select city")
    {
    alert("please select city");
    return false;
    }
    
    if((radio1=="")&&(radio2==""))
    {
         alert("select either male or female");
         return false;   
    }
    

    
    
    if (email=="")
    {
    alert("please fill your email_id");
    return false;
    }
    
    if(email.indexOf('@')<=0)
    {
        alert("@ invalid position");
        return false;
    }
    
    if((email.charAt(email.length-4)!='.' && email.charAt(email.length-3)!='.' ))
    {
        alert(" . invalid position");
        return false;
    }
    
    if (phoneno=="")
    {
    alert("please fill your phone_no");
    return false;
    }
    
    if(isNaN(phoneno))
    {
        alert("please enter only digits");
        return false;
    }
    if(phoneno.length!=10)
    {
         alert("phone number muse be 10 digit only");
         return false;
    }
}

    
    